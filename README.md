# Assignment 8

All solutions must be placed on [CodeAnywhere](https://codeanywhere.com) into the folder `a08`.
Put each problem into the corresponding file `p1.py`, `p2.py`, `p3.py` *etc*.

All files must produce *no errors* when invoked with Python *e.g.* as
```
python p1.py
```

Each file must have a
```shell
#!/usr/bin/env python3
```
line at the very top, and must be made executable with a command
```shell
chmod a+x p1.py
```
(same for `p2.py` ...)




### Problem 1 (*20%*)

Write a function `delete_entry(tpl, idx)` that takes two arguments — a tuple `tpl` and a number `idx`,
and returns a tuple identical to `tpl` but with the element number `idx` removed

*Hint:* since tuples are immutable, you will need to create a different tuple




### Problem 2 (*40%*)

Write a program which, given a tuple of numbers, say,
```python
    T = (38, 75, 27, 97, 80, 29, 57, 61, 22, 70, 92, 15, 13, 61, 60, 25, 0, 90, 19, 49)
```
splits it into two other tuples `T1` and `T2` by putting every other entry into each tuple,
that is,

* `T1` should become `(38, 27, 80, 57, ...)`

and

* `T2` should be `(75, 97, 29, ...)`

This program should work for any tuple `T` that contains an even number of entries




### Problem 3 (*40%*)

Using a list comprehension, of the type
```python
	L = [x  for x in ... ]
```
generate a list of 10 random numbers less than 20.

*Hint:* you need to import `random` module, and use `random.randrange()` function:
```python
	import random

	L = [ ... your solution ... ]
```
